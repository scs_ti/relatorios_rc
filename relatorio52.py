from functions import *
from querys import *
import json
import time
from datetime import datetime

print('======================= Relatório 52 ========================')
# %% PARAMENTROS DE CONEXÕES AOS BANCO DE DADOS
curr_path = os.path.dirname(os.path.abspath(__file__))
with open(os.path.join(curr_path, 'pass.json'), 'r') as f:
    parameters = json.load(f)

with open(os.path.join(curr_path, 'pass.json'), 'r') as f:
    parameters = json.load(f)
    user = parameters['datamart.db']['user']
    psw = parameters['datamart.db']['pass']
    host = parameters['datamart.db']['host']

# CONEXÃO DO BANCO DE TESTE MYSQL ONDE ESTA O DW
try:
    engine_dw = sqlalchemy.create_engine(
        'mysql+pymysql://{}:{}@{}:3306/prd_db_provedor'.format(user, psw, host),
        echo=False, poolclass=NullPool)
except Exception as e:
    print(e)
    sys.exit()



## INÍCIO DO PROGRAMA
print('Buscando dados do banco de produção a partir do primeiro dia do mês atual.')
with cx_Oracle.connect('{}/{}@{}'.format(parameters['prdme.db']['user'],
                                         parameters['prdme.db']['pass'],
                                         parameters['prdme.db']['host'])) as connection:
    df_resumo = pd.read_sql(sql_52, connection)

print('Busca ok!')
print('Carregando os dados na tabela resumo_re.')

print('deletando registros a partir do primeiro dia do mês atual da tabela dw_re')
with engine_dw.connect() as con:
    result = con.execute(sql_delete_52)
print('deletado com sucesso.')

df_resumo.to_sql('relatorio52', con=engine_dw, if_exists='append', index=False)
print('Carregamento finalizado')
time.sleep(3)

data_execucao = datetime.today().strftime('%d/%m/%Y %H:%M:00')
data = datetime.strptime(data_execucao, '%d/%m/%Y %H:%M:00')

with engine_dw.connect() as con:
    con.execute('''DELETE FROM historico_execucao H WHERE H.script = 'Relatorio52_RC' ''')
    con.execute(
        '''INSERT INTO historico_execucao (data_execucao, script) VALUES ('{}', 'Relatorio52_RC')'''.format(data))

# DESTA LINHA PARA BAIXO, É UTILIZADA PARA REPOPULAR TODA A TABELA DO BANCO
# Pegando primeiro dia e último  de cada mês
# with cx_Oracle.connect('{}/{}@{}'.format(parameters['prdme.db']['user'],
#                                          parameters['prdme.db']['pass'],
#                                          parameters['prdme.db']['host'])) as connection:
#     df_calendar = pd.read_sql(sql_calendario, connection)
#
#
# df_calendar['PRIMEIRO_DIA_MES'] = df_calendar['PRIMEIRO_DIA_MES'].dt.date
# df_calendar['ULTIMO_DIA_MES'] = df_calendar['ULTIMO_DIA_MES'].dt.date
#
# df_calendar['PRIMEIRO_DIA_MES'] = df_calendar['PRIMEIRO_DIA_MES'].apply(lambda x: x.strftime('%d-%m-%Y'))
# df_calendar['ULTIMO_DIA_MES'] = df_calendar['ULTIMO_DIA_MES'].apply(lambda x: x.strftime('%d-%m-%Y'))
#
# df_calendar = df_calendar.sort_values(by=['ID'],ascending=True)
# for i, r in df_calendar.iterrows():
#     with cx_Oracle.connect('{}/{}@{}'.format(parameters['prdme.db']['user'],
#                                              parameters['prdme.db']['pass'],
#                                              parameters['prdme.db']['host'])) as connection:
#         df_res_append = pd.read_sql(sql_52.format(r[1],r[2]), connection)
#     print(r[1], r[2], "carregado")
#     df_res_append.to_sql('relatorio52', con=engine_dw, if_exists='append', index=False)
