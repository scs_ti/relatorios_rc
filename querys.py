sql_274 = '''
SELECT DC.CD_CONTRATO  AS "CODIGO_CONTRATO",
       C.NM_RESPONSAVEL_FINANCEIRO AS "CONTRATO",
       C.TP_CONTRATO AS "TIPO_CONTRATO",
       C.SN_DEMITIDO_APOSENTADO_OBITO AS "DEMITIDO_APOSENTADO",
       C.CD_USUARIO    AS "COD_RE",
       RE.NM_USUARIO   AS "RE_RESPONSAVEL",
       C.CD_EMPRESA    AS "CODIGO_EMPRESA",
       E.DS_EMPRESA    AS "EMPRESA",
       TO_CHAR (C.DT_VENDA, 'DD/MM/YYYY') AS "DATA_VENDA_CONTRATO",
       TO_CHAR (DC.DT_DESLIGAMENTO, 'DD/MM/YYYY') AS "DATA_DESLIGAMENTO_CONTRATO",
       DC.CD_MOT_DESLIGAMENTO AS "CODIGO_MOTIVO",
       M.DS_MOT_DESLIGAMENTO  AS "MOTIVO_DESLIGAMENTO",
       (SELECT COUNT(CD_MATRICULA) FROM DBAPS.DESLIGAMENTO T
        WHERE T.CD_CONTRATO = DC.CD_CONTRATO
         AND  TRUNC( T.DT_DESLIGA_CONTRATO) = TRUNC(DC.DT_DESLIGAMENTO)) "VIDAS_CANCELADAS",

       -- MES CANCELAMENTO
       NVL((SELECT SUM (R."VALOR PAGO") FROM DBAPS.V_HEALTHBIT_RESUMO R WHERE R."NR CONTRATO" = C.CD_CONTRATO
                        AND R."MES REFERENCIA" || '/' || R."ANO REFERENCIA" = TO_CHAR (DC.DT_DESLIGAMENTO, 'MM/YYYY')),0) "VALOR_PAGO_MES_CANCELADO",

       NVL((SELECT SUM (R."VALOR ITEM MENSALIDADE") FROM DBAPS.V_HEALTHBIT_RESUMO R WHERE R."NR CONTRATO" = C.CD_CONTRATO
                        AND R."MES REFERENCIA" || '/' || R."ANO REFERENCIA" = TO_CHAR (DC.DT_DESLIGAMENTO, 'MM/YYYY')),0) "FATURADO_MENS_MES_CANCELADO",

       NVL((SELECT SUM (R."VALOR ITEM COPART") FROM DBAPS.V_HEALTHBIT_RESUMO R WHERE R."NR CONTRATO" = C.CD_CONTRATO
                        AND R."MES REFERENCIA" || '/' || R."ANO REFERENCIA" = TO_CHAR (DC.DT_DESLIGAMENTO, 'MM/YYYY')),0) "FATURADO_COPART_MES_CANCELADO",

       NVL((SELECT SUM (R."VALOR DO PREMIO") FROM DBAPS.V_HEALTHBIT_RESUMO R WHERE R."NR CONTRATO" = C.CD_CONTRATO
                        AND R."MES REFERENCIA" || '/' || R."ANO REFERENCIA" = TO_CHAR (DC.DT_DESLIGAMENTO, 'MM/YYYY')),0) "FATURADO_TOTAL_MES_CANCELADO",

       NVL((SELECT SUM (R."VALOR DO SINISTRO") FROM DBAPS.V_HEALTHBIT_RESUMO R WHERE R."NR CONTRATO" = C.CD_CONTRATO
                        AND R."MES REFERENCIA" || '/' || R."ANO REFERENCIA" = TO_CHAR (DC.DT_DESLIGAMENTO, 'MM/YYYY')),0) "VALOR_SINISTRO_MES_CANCELADO",
      -- ANO
       NVL((SELECT SUM (R."VALOR PAGO") FROM DBAPS.V_HEALTHBIT_RESUMO R WHERE R."NR CONTRATO" = C.CD_CONTRATO
                        AND  MONTHS_BETWEEN ( TRUNC (DC.DT_DESLIGAMENTO), TO_DATE ('01/' ||R."MES REFERENCIA" || '/' || R."ANO REFERENCIA", 'DD/MM/YYYY')) <= 12
                        AND MONTHS_BETWEEN ( TRUNC (DC.DT_DESLIGAMENTO), TO_DATE ('01/' ||R."MES REFERENCIA" || '/' || R."ANO REFERENCIA", 'DD/MM/YYYY')) >= 0   ),0) "VALOR_PAGO_12_COMP",

       NVL((SELECT SUM (R."VALOR ITEM MENSALIDADE") FROM DBAPS.V_HEALTHBIT_RESUMO R WHERE R."NR CONTRATO" = C.CD_CONTRATO
                        AND  MONTHS_BETWEEN ( TRUNC (DC.DT_DESLIGAMENTO), TO_DATE ('01/' ||R."MES REFERENCIA" || '/' || R."ANO REFERENCIA", 'DD/MM/YYYY')) <= 12
                        AND MONTHS_BETWEEN ( TRUNC (DC.DT_DESLIGAMENTO), TO_DATE ('01/' ||R."MES REFERENCIA" || '/' || R."ANO REFERENCIA", 'DD/MM/YYYY')) >= 0   ),0) "FATURADO_MENS_12_COMP",

       NVL((SELECT SUM (R."VALOR ITEM COPART") FROM DBAPS.V_HEALTHBIT_RESUMO R WHERE R."NR CONTRATO" = C.CD_CONTRATO
                        AND  MONTHS_BETWEEN ( TRUNC (DC.DT_DESLIGAMENTO), TO_DATE ('01/' ||R."MES REFERENCIA" || '/' || R."ANO REFERENCIA", 'DD/MM/YYYY')) <= 12
                        AND MONTHS_BETWEEN ( TRUNC (DC.DT_DESLIGAMENTO), TO_DATE ('01/' ||R."MES REFERENCIA" || '/' || R."ANO REFERENCIA", 'DD/MM/YYYY')) >= 0   ),0) "FATURADO_COPART_12_COMP",

       NVL((SELECT SUM (R."VALOR DO PREMIO") FROM DBAPS.V_HEALTHBIT_RESUMO R WHERE R."NR CONTRATO" = C.CD_CONTRATO
                        AND  MONTHS_BETWEEN ( TRUNC (DC.DT_DESLIGAMENTO), TO_DATE ('01/' ||R."MES REFERENCIA" || '/' || R."ANO REFERENCIA", 'DD/MM/YYYY')) <= 12
                        AND MONTHS_BETWEEN ( TRUNC (DC.DT_DESLIGAMENTO), TO_DATE ('01/' ||R."MES REFERENCIA" || '/' || R."ANO REFERENCIA", 'DD/MM/YYYY')) >= 0   ),0) "FATURADO_TOTAL_12_COMP",

       NVL((SELECT SUM (R."VALOR DO SINISTRO") FROM DBAPS.V_HEALTHBIT_RESUMO R WHERE R."NR CONTRATO" = C.CD_CONTRATO
                        AND  MONTHS_BETWEEN ( TRUNC (DC.DT_DESLIGAMENTO), TO_DATE ('01/' ||R."MES REFERENCIA" || '/' || R."ANO REFERENCIA", 'DD/MM/YYYY')) <= 12
                        AND MONTHS_BETWEEN ( TRUNC (DC.DT_DESLIGAMENTO), TO_DATE ('01/' ||R."MES REFERENCIA" || '/' || R."ANO REFERENCIA", 'DD/MM/YYYY')) >= 0   ),0) "VALOR_SINISTRO_12_COMP",
        DBAPS.FNC_CLASSIFICA_MOT_CANCEL(DC.CD_MOT_DESLIGAMENTO,'RESUMO_MOTIVO') AS "RESUMO_MOTIVO",
        DBAPS.FNC_CLASSIFICA_MOT_CANCEL(DC.CD_MOT_DESLIGAMENTO,'CLASSIFICACAO') AS "CLASSIFICACAO"

FROM DBAPS.DESLIGA_CONTRATO DC

INNER JOIN DBAPS.CONTRATO   C  ON C.CD_CONTRATO = DC.CD_CONTRATO
LEFT  JOIN DBAPS.EMPRESA    E  ON E.CD_EMPRESA  = C.CD_EMPRESA
LEFT  JOIN DBAPS.MOT_DESLIG M  ON M.CD_MOT_DESLIGAMENTO = DC.CD_MOT_DESLIGAMENTO
LEFT  JOIN DBASGU.USUARIOS  RE ON RE.CD_USUARIO = C.CD_USUARIO
WHERE
 TRUNC(DC.DT_DESLIGAMENTO) >= trunc(last_day(add_months(sysdate, -1)) + 1)
 --TRUNC(DC.DT_DESLIGAMENTO) >= TO_DATE(TO_CHAR(TRUNC(sysdate) - 'colchetes','DD/MM/YYYY'),'DD/MM/YYYY')
     --TRUNC(DC.DT_DESLIGAMENTO) between to_date('colchetes','dd/mm/yyyy') and  to_date('colchetes','dd/mm/yyyy')
 AND  DC.DT_REATIVACAO IS NULL
 AND C.SN_DEMITIDO_APOSENTADO_OBITO = 'N'
 AND C.CD_CONTRATO_TEM is null
 AND  C.TP_CONTRATO <> 'I'
'''


sql_106 = '''
SELECT
       CASE U.SN_TITULAR
         WHEN 'S' THEN TO_CHAR(U.CD_MATRICULA)
         WHEN 'N' THEN TO_CHAR(U.CD_MATRICULA_TEM)
       END                   AS "CODIGO_TITULAR",
       CASE U.SN_TITULAR
         WHEN 'S' THEN U.NM_SEGURADO
         WHEN 'N' THEN (SELECT T.NM_SEGURADO FROM DBAPS.USUARIO T WHERE T.CD_MATRICULA = U.CD_MATRICULA_TEM)
       END                   AS "TITULAR",
       TO_CHAR(D.CD_MOT_DESLIGAMENTO) AS "COD_MOTIVO_DESLIGAMENTO",
       M.DS_MOT_DESLIGAMENTO AS "MOTIVO_DESLIGAMENTO",
       TO_CHAR(U.CD_EMPRESA) AS "CODIGO_EMPRESA",
       E.DS_EMPRESA          AS "EMPRESA",
       U.CD_CONTRATO         AS "CODIGO_CONTRATO",
       C.NM_RESPONSAVEL_FINANCEIRO AS "RESP_CONTRATO",
       C.CD_CONTRATO_CATEGORIA                    "COD_CATEGORIA_CONTRATO",
       CT.DS_CONTRATO_CATEGORIA                   "CATEGORIA_CONTRATO",
       C.DT_VENDA AS "DT_VENDA_CONTRATO",
       (SELECT TO_CHAR (DC.DT_DESLIGAMENTO, 'DD/MM/YYYY') FROM DBAPS.DESLIGA_CONTRATO DC WHERE DC.CD_CONTRATO = C.CD_CONTRATO AND DC.DT_REATIVACAO IS NULL AND ROWNUM = 1) AS "DT_CANCELAMENTO_CONTRATO",
       TO_CHAR(U.CD_MATRICULA)        AS "CODIGO_BENEFICIARIO",
       U.NM_SEGURADO         AS "BENEFICIARIO",
       D.DT_DESLIGAMENTO     AS "DT_DESLIGAMENTO",
       U.DT_CADASTRO         AS "DT_INCLUSAO_BENEFICIARIO",
       U.NR_TELEFONE         AS "TELEFONE",
       U.NM_CIDADE           AS "CIDADE",
       FNC_VALOR_BENEFICIARIO (U.CD_MATRICULA, SYSDATE, NULL, 'N', NULL) AS "VALOR_MENSALIADE",
       D.CD_USUARIO_DESLIGAMENTO AS "OPERADOR_CANCELAMENTO",
       C.TP_CONTRATO             AS "TIPO_CONTRATO",
       C.SN_DEMITIDO_APOSENTADO_OBITO AS "DEMITIDO_APOSENTADO",
        DBAPS.FNC_CLASSIFICA_MOT_CANCEL(D.CD_MOT_DESLIGAMENTO,'RESUMO_MOTIVO') AS "RESUMO_MOTIVO",
        DBAPS.FNC_CLASSIFICA_MOT_CANCEL(D.CD_MOT_DESLIGAMENTO,'CLASSIFICACAO') AS "CLASSIFICACAO"

FROM DBAPS.DESLIGAMENTO     D
INNER JOIN DBAPS.USUARIO    U ON U.CD_MATRICULA        = D.CD_MATRICULA
LEFT  JOIN DBAPS.EMPRESA    E ON E.CD_EMPRESA          = U.CD_EMPRESA
INNER  JOIN DBAPS.CONTRATO   C ON C.CD_CONTRATO         = U.CD_CONTRATO
LEFT  JOIN DBAPS.MOT_DESLIG M ON M.CD_MOT_DESLIGAMENTO = D.CD_MOT_DESLIGAMENTO
LEFT JOIN DBAPS.CONTRATO_CATEGORIA CT ON CT.CD_CONTRATO_CATEGORIA = C.CD_CONTRATO_CATEGORIA
WHERE 
 TRUNC(D.DT_DESLIGAMENTO) >= trunc(last_day(add_months(sysdate, -1)) + 1)
 --TRUNC(D.DT_DESLIGAMENTO) >= TO_DATE(TO_CHAR(TRUNC(sysdate) - 'COLCHETES','DD/MM/YYYY'),'DD/MM/YYYY')
     --TRUNC(D.DT_DESLIGAMENTO) between to_date('COLCHETES','dd/mm/yyyy') and  to_date('COLCHETES','dd/mm/yyyy')
 AND  D.DT_REATIVACAO IS NULL

'''

sql_343 = '''
SELECT DC.CD_CONTRATO                                                                                               AS "CODIGO_CONTRATO",
       C.NM_RESPONSAVEL_FINANCEIRO                                                                                  AS "CONTRATO",
       C.TP_CONTRATO                                                                                                AS "TIPO_CONTRATO",
       C.SN_DEMITIDO_APOSENTADO_OBITO                                                                               AS "DEMITIDO_APOSENTADO",
       C.CD_USUARIO                                                                                                 AS "COD_RE",
       RE.NM_USUARIO                                                                                                AS "RE_RESPONSAVEL",
       C.CD_EMPRESA                                                                                                 AS "CODIGO_EMPRESA",
       E.DS_EMPRESA                                                                                                 AS "EMPRESA",
       TO_CHAR (C.DT_VENDA, 'DD/MM/YYYY')                                                                           AS "DATA_VENDA_CONTRATO",
       TO_CHAR (DC.DT_DESLIGAMENTO, 'DD/MM/YYYY')                                                                   AS "DATA_DESLIGAMENTO_CONTRATO",
       DC.CD_MOT_DESLIGAMENTO                                                                                       AS "CODIGO_MOTIVO",
       M.DS_MOT_DESLIGAMENTO                                                                                        AS "MOTIVO_DESLIGAMENTO",
       DBAPS.FNC_CLASSIFICA_MOT_CANCEL(DC.CD_MOT_DESLIGAMENTO,'RESUMO_MOTIVO')                                      AS "RESUMO_MOTIVO",
       DBAPS.FNC_CLASSIFICA_MOT_CANCEL(DC.CD_MOT_DESLIGAMENTO,'CLASSIFICACAO')                                      AS "CLASSIFICACAO",

       (SELECT COUNT(CD_MATRICULA) FROM DBAPS.DESLIGAMENTO T
        WHERE T.CD_CONTRATO = DC.CD_CONTRATO
         AND  TRUNC( T.DT_DESLIGA_CONTRATO) = TRUNC(DC.DT_DESLIGAMENTO))                                            AS  "VIDAS CANCELADAS",
      NVL((SELECT SUM (FNC_VALOR_BENEFICIARIO (DU.CD_MATRICULA, T.DT_DESLIGAMENTO, DU.NR_IDADE_FIXADA, CU.SN_DEMITIDO_APOSENTADO_OBITO, NULL)) FROM DBAPS.DESLIGAMENTO T
        INNER JOIN DBAPS.USUARIO DU ON DU.CD_MATRICULA = T.CD_MATRICULA
        INNER JOIN DBAPS.CONTRATO CU ON CU.CD_CONTRATO = DU.CD_CONTRATO
        WHERE T.CD_CONTRATO = DC.CD_CONTRATO
    AND  TRUNC( T.DT_DESLIGA_CONTRATO) = TRUNC(DC.DT_DESLIGAMENTO)),0)                                              AS "VALOR_CANCELADAS"  ,
        DBAPS.FNC_RETORNA_MES_BI(TO_CHAR(DC.DT_DESLIGAMENTO, 'MM'),'S') ||'/'|| TO_CHAR(DC.DT_DESLIGAMENTO, 'YYYY') AS "MES_REFERENCIA",
        C.SN_DEMITIDO_APOSENTADO_OBITO,
        C.CD_CONTRATO_TEM,
        C.TP_CONTRATO
FROM DBAPS.DESLIGA_CONTRATO DC
INNER JOIN DBAPS.CONTRATO   C  ON C.CD_CONTRATO = DC.CD_CONTRATO
LEFT  JOIN DBAPS.EMPRESA    E  ON E.CD_EMPRESA  = C.CD_EMPRESA
LEFT  JOIN DBAPS.MOT_DESLIG M  ON M.CD_MOT_DESLIGAMENTO = DC.CD_MOT_DESLIGAMENTO
LEFT  JOIN DBASGU.USUARIOS  RE ON RE.CD_USUARIO = C.CD_USUARIO
WHERE
     TRUNC(DC.DT_DESLIGAMENTO) >= trunc(last_day(add_months(sysdate, -1)) + 1)
     --TRUNC(DC.DT_DESLIGAMENTO) >= TO_DATE(TO_CHAR(TRUNC(sysdate) - '{}','DD/MM/YYYY'),'DD/MM/YYYY')
     --TRUNC(DC.DT_DESLIGAMENTO) between to_date('COLCHETES','dd/mm/yyyy') and  to_date('COLCHETES','dd/mm/yyyy')
 AND  DC.DT_REATIVACAO IS NULL
'''

sql_52 = '''
SELECT CASE U.SN_TITULAR
           WHEN 'S' THEN TO_CHAR(U.CD_MATRICULA)
           WHEN 'N' THEN TO_CHAR(U.CD_MATRICULA_TEM)
           END                                                                                     AS "CODIGO_TITULAR",
       CASE U.SN_TITULAR
           WHEN 'S' THEN U.NM_SEGURADO
           WHEN 'N' THEN (SELECT T.NM_SEGURADO FROM DBAPS.USUARIO T WHERE T.CD_MATRICULA = U.CD_MATRICULA_TEM)
           END                                                                                     AS "TITULAR",
       TO_CHAR(D.CD_MOT_DESLIGAMENTO)                                                              AS "CODIGO_MOTIVO_DESLIGAMENTO",
       M.DS_MOT_DESLIGAMENTO                                                                       AS "MOTIVO_DESLIGAMENTO",
       DBAPS.FNC_CLASSIFICA_MOT_CANCEL(D.CD_MOT_DESLIGAMENTO,'RESUMO_MOTIVO') AS "RESUMO_MOTIVO",
       DBAPS.FNC_CLASSIFICA_MOT_CANCEL(D.CD_MOT_DESLIGAMENTO,'CLASSIFICACAO') AS "CLASSIFICACAO",
       TO_CHAR(U.CD_EMPRESA)                                                                       AS "CODIGO_EMPRESA",
       E.DS_EMPRESA                                                                                AS "EMPRESA",
       TO_CHAR(U.CD_CONTRATO)                                                                      AS "CODIGO_CONTRATO",
       C.NM_RESPONSAVEL_FINANCEIRO                                                                 AS "RESP_CONTRATO",
       (SELECT TO_CHAR(DC.DT_DESLIGAMENTO, 'DD/MM/YYYY')
        FROM DBAPS.DESLIGA_CONTRATO DC
        WHERE DC.CD_CONTRATO = C.CD_CONTRATO
          AND DC.DT_REATIVACAO IS NULL
          AND ROWNUM = 1)                                                                          AS "DATA_DESLIGA_CONTRATO",
       C.DT_VENDA                                                                                  AS "DATA_VENDA_CONTRATO",
       TO_CHAR(U.CD_MATRICULA)                                                                     AS "CODIGO_BENEFICIARIO",
       U.NM_SEGURADO                                                                               AS "BENEFICIARIO",
       TO_CHAR(U.CD_EMP_VENDEDORA) || ' - ' || EU.DS_EMP_VENDEDORA                                    "CORRETORA_BENEFICIARIO",
       TO_CHAR(U.CD_VENDEDOR) || ' - ' || VU.NM_VENDEDOR                                              "VENDEDOR_BENEFICIARIO",
       TO_CHAR (D.DT_DESLIGAMENTO, 'DD/MM/YYYY')                                                   AS "DATA_DESLIGAMENTO",
       U.DT_CADASTRO                                                                               AS "DATA_INCLUSAO_BENEFICIARIO",
       U.NR_TELEFONE                                                                               AS "TELEFONE",
       U.NM_CIDADE                                                                                 AS "CIDADE",
       FNC_VALOR_BENEFICIARIO(U.CD_MATRICULA, SYSDATE, NULL, C.SN_DEMITIDO_APOSENTADO_OBITO,
                              NULL)                                                                AS "VALOR_MENSALIADE",
       D.CD_USUARIO_DESLIGAMENTO                                                                   AS "OPERADOR_CANCELAMENTO",
       C.TP_CONTRATO                                                                               AS "TIPO_CONTRATO",
       U.SN_ATIVO                                                                                  AS "STATUS_BENEFICIARIO",
       C.SN_DEMITIDO_APOSENTADO_OBITO                                                              AS "APOSENTADO/DEMITIDO",
       TO_CHAR(C.CD_EMP_VENDEDORA) || ' - ' || EC.DS_EMP_VENDEDORA                                    "CORRETORA_CONTRATO",
       TO_CHAR(C.CD_VENDEDOR) || ' - ' || VC.NM_VENDEDOR                                              "VENDEDOR_CONTRATO",
       (SELECT COUNT(M.CD_MENS_CONTRATO)
        FROM DBAPS.MENS_CONTRATO M
        WHERE M.CD_MOTIVO_CANCELAMENTO IS NULL
          AND M.TP_QUITACAO IN ('P', 'Q')
          AND M.CD_CONTRATO = NVL(NVL(C.CD_CONTRATO, C.CD_CONTRATO_TEM),
                                  C.CD_CONTRATO_RESP_COBRANCA))                                       "QT_TITULOS_PAGOS_DO_CONTRATO",
       U.CD_PLANO                                                                                  AS "CODIGO_PLANO",
       P.DS_PLANO                                                                                  AS "PLANO",
       P.CD_REGISTRO_MS                                                                            AS "REGISTRO_ANS",
       P.CD_PLANO_OPERADORA                                                                        AS "SCPA",
       DECODE(P.TP_PERIODO_IMPLANTACAO, 1, 'N', 2, 'S')                                            AS "REGULAMENTADO",
       DBAPS.FNC_RETORNA_MES_BI(TO_CHAR(D.DT_DESLIGAMENTO, 'MM'),'S') ||'/'|| TO_CHAR(D.DT_DESLIGAMENTO, 'YYYY') AS "ANO_MES"
FROM DBAPS.DESLIGAMENTO D
         INNER JOIN DBAPS.USUARIO U ON U.CD_MATRICULA = D.CD_MATRICULA
         LEFT JOIN DBAPS.PLANO P ON P.CD_PLANO = U.CD_PLANO
         LEFT JOIN DBAPS.EMPRESA E ON E.CD_EMPRESA = U.CD_EMPRESA
         INNER JOIN DBAPS.CONTRATO C ON C.CD_CONTRATO = U.CD_CONTRATO
         LEFT JOIN DBAPS.MOT_DESLIG M ON M.CD_MOT_DESLIGAMENTO = D.CD_MOT_DESLIGAMENTO
         LEFT JOIN DBAPS.EMP_VENDEDORA EU ON EU.CD_EMP_VENDEDORA = U.CD_EMP_VENDEDORA
         LEFT JOIN DBAPS.VENDEDOR VU ON VU.CD_VENDEDOR = U.CD_VENDEDOR
         LEFT JOIN DBAPS.EMP_VENDEDORA EC ON EC.CD_EMP_VENDEDORA = C.CD_EMP_VENDEDORA
         LEFT JOIN DBAPS.VENDEDOR VC ON VC.CD_VENDEDOR = C.CD_VENDEDOR
WHERE TRUNC(D.DT_DESLIGAMENTO) >= trunc(last_day(add_months(sysdate, -1)) + 1)
--TRUNC(D.DT_DESLIGAMENTO) >= TO_DATE(TO_CHAR(TRUNC(sysdate) - 'colchetes', 'DD/MM/YYYY'), 'DD/MM/YYYY')
      --TRUNC(D.DT_DESLIGAMENTO) between to_date('colchetes','dd/mm/yyyy') and  to_date('colchetes','dd/mm/yyyy')
  AND D.DT_REATIVACAO IS NULL
'''



sql_calendario = '''select * from CALENDARIO_BI C
WHERE C.PRIMEIRO_DIA_MES <= TO_DATE(SYSDATE,'DD/MM/YY')
and trunc(C.PRIMEIRO_DIA_MES) >= TO_DATE('01/01/2019','DD/MM/YY') '''



sql_delete_274 = '''
DELETE
    FROM relatorio274 R
    WHERE
         date_format(str_to_date(R.DATA_DESLIGAMENTO_CONTRATO, '%%d/%%m/%%Y'),'%%Y/%%m/%%d')
          >= DATE_FORMAT(last_day(CURDATE()-INTERVAL 1 month) + interval 1 day,'%%Y/%%m/%%d')
        '''

sql_delete_106 = '''
DELETE
    FROM relatorio106 r
    WHERE
          date_format(R.DT_DESLIGAMENTO,'%%Y/%%m/%%d') >= DATE_FORMAT(last_day(CURDATE()-INTERVAL 1 month) + interval 1 day,'%%Y/%%m/%%d')
        '''

sql_delete_343 = '''
DELETE
    FROM relatorio343 R
    WHERE
         date_format(str_to_date(R.DATA_DESLIGAMENTO_CONTRATO, '%%d/%%m/%%Y'),'%%Y/%%m/%%d')
          >= DATE_FORMAT(last_day(CURDATE()-INTERVAL 1 month) + interval 1 day,'%%Y/%%m/%%d')
        '''


sql_delete_52 = '''
DELETE
    FROM relatorio52 R
    WHERE
         date_format(str_to_date(R.DATA_DESLIGAMENTO, '%%d/%%m/%%Y'),'%%Y/%%m/%%d')
          >= DATE_FORMAT(last_day(CURDATE()-INTERVAL 1 month) + interval 1 day,'%%Y/%%m/%%d')
        '''


